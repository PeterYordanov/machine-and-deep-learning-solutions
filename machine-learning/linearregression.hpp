#ifndef LINEARREGRESSION_HPP
#define LINEARREGRESSION_HPP

#include <vector>

class LinearRegression
{
public:
	LinearRegression(const std::vector<double>& x_vals, const std::vector<double>& y_vals);
	~LinearRegression(){}

	void train(int iterations, double a_coefficient, double b_coefficient);
	double regress(double x);

private:
	bool isConverged();
	std::vector<double> m_x_values;
	std::vector<double> m_y_values;
	double m_num_elements;
	double m_a_coefficient;
	double m_b_coefficient;
	double m_old_error;
};

#endif // LINEARREGRESSION_HPP
