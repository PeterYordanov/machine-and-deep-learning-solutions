#include "kmeans.hpp"
#include <QDebug>

KMeans::KMeans(int k, const std::vector<std::pair<double, double>>& data)
	: m_k(k),
	  m_means(k),
	  m_data(k)
{
	m_data[0] = data;
}

void KMeans::cluster(std::valarray<std::pair<double, double>>* init_means, int num_iters)
{
	m_means = *init_means;
	assignLabels();

	int i = 0;
	while(i < num_iters) {
		qDebug() << "Running iteration: " << i;
		computeMeans();
		assignLabels();
		i++;
	}
}

bool KMeans::computeMeans()
{
	bool res = true;
	for(int i = 0; i < m_k; i++) {
		std::pair<double, double> mean(0, 0);
		int num_features_of_k = m_data[i].size();
		for(auto const& it : m_data[i]) {
			std::get<0>(mean) += std::get<0>(it);
			std::get<1>(mean) += std::get<1>(it);
		}

		std::get<0>(mean) /= num_features_of_k;
		std::get<1>(mean) /= num_features_of_k;
		//is it converged?
		res = (m_means[i] == mean && res == true) ? true : false;
		m_means[i] = mean;
		qDebug() << "Cluster Centroid: " << m_k << " :\tx " << std::get<0>(mean) << " ,y " << std::get<1>(mean);
	}

	return res;
}

void KMeans::assignLabels()
{
	std::valarray<std::vector<std::pair<double, double>>> new_data(m_k);
	for(auto const& clust : m_data) {
		for(auto const& feature : clust) {
			int closest_mean = computeClosestCentroid(feature);
			new_data[closest_mean].push_back(feature);
		}
	}

	m_data = new_data;
}

int KMeans::computeClosestCentroid(const std::pair<double, double>& point)
{
	std::valarray<double> distances(m_k);
	for(int k = 0; k < m_k; k++) {
		double del_x = std::get<0>(point) - std::get<0>(m_means[k]);
		double del_y = std::get<1>(point) - std::get<1>(m_means[k]);
		double dist = std::sqrt((del_x * del_x) + (del_y * del_y));
		distances[k] = dist;
	}
	//closest mean
	return std::distance(std::begin(distances), std::min_element(std::begin(distances), std::end(distances)));
}

void KMeans::printClusters() const
{
	for(int k = 0; k < m_k; k++) {
		for(const auto& it : m_data[k]) {
			qDebug() << " [" << std::get<0>(it) << " , " << std::get<1>(it) << "] ";
		}
	}
}
