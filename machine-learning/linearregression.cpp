#include "linearregression.hpp"
#include <QDebug>
#include <cmath>
#include <limits>

LinearRegression::LinearRegression(const std::vector<double>& x_vals, const std::vector<double>& y_vals)
	: m_x_values(x_vals),
	  m_y_values(y_vals),
	  m_num_elements(y_vals.size()),
	  m_old_error(std::numeric_limits<double>::max())
{

}

void LinearRegression::train(int iterations, double a_coefficient, double b_coefficient)
{
	int iter = 0;
	m_a_coefficient = a_coefficient;
	m_b_coefficient = b_coefficient;

	while(!isConverged() && iter < iterations) {
		//update the gradient
		double step_size = 2 / double(iter + 2); //decrease at each step
		double a_grad = 0;
		double b_grad = 0;

		//compute the gradient of error to a
		for(int i = 0; i < m_num_elements; i++) {
			a_grad += m_x_values[i] * ((m_a_coefficient * m_x_values[i] + m_b_coefficient) - m_y_values[i]);
		}
		a_grad = (2 * a_grad) / m_num_elements;

		//compute the gradient of error to b
		for(int i = 0; i < m_num_elements; i++) {
			b_grad += ((m_a_coefficient * m_x_values[i] + m_b_coefficient) - m_y_values[i]);
		}
		b_grad = (2 * b_grad) / m_num_elements;

		//take a step
		m_a_coefficient = m_a_coefficient - (step_size * a_grad);
		m_b_coefficient = m_b_coefficient - (step_size * b_grad);

		qDebug() << "a:\t" << m_a_coefficient << ", b:\t" << m_b_coefficient;
		qDebug() << "grad_a:\t" << a_grad << ", grad_b\t" << b_grad << "\n";
		iter++;
	}
}

double LinearRegression::regress(double x)
{
	return m_a_coefficient * x + m_b_coefficient;
}

bool LinearRegression::isConverged()
{
	double error = 0;
	double threshold = 0.001;

	for(int i = 0; i < m_num_elements; i++) {
		error += ((m_a_coefficient * m_x_values[i] + m_b_coefficient) - m_y_values[i]) * ((m_a_coefficient * m_x_values[i] + m_b_coefficient) - m_y_values[i]);
	}
	error /= m_num_elements;
	qDebug() << "Error: " << error;

	bool result = (std::abs(error) > m_old_error - threshold && std::abs(error) < m_old_error + threshold) ? true : false;
	m_old_error = std::abs(error);

	return result;
}
