#ifndef KMEANS_HPP
#define KMEANS_HPP

#include <valarray>
#include <vector>

class KMeans
{
public:
	KMeans(int k, const std::vector<std::pair<double, double>>& data);
	~KMeans() = default;

	void cluster(std::valarray<std::pair<double, double>>* init_means, int num_iters);
	void printClusters() const;

private:
	bool computeMeans();
	void assignLabels();
	int computeClosestCentroid(const std::pair<double, double>& point);

	int m_k;
	int m_features; //num of data points
	std::valarray<std::pair<double, double>> m_means; //container to hold the current means
	std::valarray<std::vector<std::pair<double, double>>> m_data;
};

#endif // KMEANS_HPP
