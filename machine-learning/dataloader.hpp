#ifndef DATALOADER_HPP
#define DATALOADER_HPP

#include <vector>
#include <valarray>

class DataLoader
{
public:
	DataLoader();

	static inline std::vector<double> loadLinearRegressionX()
	{ return std::vector<double>({1, 2, 3, 4, 5}); }
	static inline std::vector<double> loadLinearRegressionY()
	{ return std::vector<double>({2.8, 2.9, 7.6, 9, 8.6}); }

	static inline std::vector<std::pair<double, double>> kmeansData()
	{
		return std::vector<std::pair<double, double>>
			({ {1.1, 1}, {1.4, 2}, {3.8, 7}, {5.0, 8}, {4.3, 6},
				{8, 5.0}, {6, 8.5}, {3, 2.0}, {9, 6}, {9.1, 4} });
	}
};

#endif // DATALOADER_HPP
