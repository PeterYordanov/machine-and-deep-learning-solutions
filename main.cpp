#include "mainwindow.hpp"
#include <QApplication>
#include "machine-learning/dataloader.hpp"
#include "machine-learning/linearregression.hpp"
#include "machine-learning/kmeans.hpp"
#include <QDebug>

int main(int argc, char *argv[])
{
	qDebug() << "=====================================";
	qDebug() << "Linear Regression";
	qDebug() << "=====================================";
	LinearRegression linearRegression(DataLoader::loadLinearRegressionX(), DataLoader::loadLinearRegressionY());
	linearRegression.train(1000, 3, -10);
	qDebug() << linearRegression.regress(3);

	qDebug() << "\n\n=====================================";
	qDebug() << "K-Means";
	qDebug() << "=====================================";
	KMeans kmeans(3, DataLoader::kmeansData());
	std::valarray<std::pair<double, double>> init_means = { {1, 1}, {3, 4}, {8, 8} };

	kmeans.cluster(&init_means, 10);
	kmeans.printClusters();

	return 0;
}
